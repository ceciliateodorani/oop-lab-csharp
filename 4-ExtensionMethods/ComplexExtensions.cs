﻿using System;

namespace ExtensionMethods {

    public static class ComplexExtensions
    {
        public static IComplex Add(this IComplex c1, IComplex c2)
        {
            return new Complex(c1.Real + c2.Real, c1.Imaginary + c2.Imaginary);
        }

        public static IComplex Subtract(this IComplex c1, IComplex c2)
        {
            return new Complex(-c1.Real,-c1.Imaginary);
        }

        public static IComplex Multiply(this IComplex c1, IComplex c2)
        {
            return new Complex((c1.Real*c2.Real)-(c1.Imaginary*c2.Imaginary),
                                (c1.Real*c2.Imaginary)+(c1.Imaginary*c2.Real));
        }

        public static IComplex Divide(this IComplex c1, IComplex c2)
        {
            return new Complex(((c1.Real * c2.Real)+ (c1.Imaginary * c2.Imaginary)) 
                                    / (Math.Pow(c2.Real,2)+Math.Pow(c2.Imaginary,2)),
                                    ((c1.Imaginary * c2.Real)- (c1.Real * c2.Imaginary))
                                    /(Math.Pow(c2.Real, 2) + Math.Pow(c2.Imaginary, 2)));
        }

        public static IComplex Conjugate(this IComplex c1)
        {
            return new Complex(c1.Real,-c1.Imaginary);
        }

        public static IComplex Reciprocal(this IComplex c1)
        {
            return new Complex(c1.Real/(Math.Pow(c1.Real,2)+Math.Pow(c1.Imaginary,2)),
                                (-c1.Imaginary)/ (Math.Pow(c1.Real, 2) + Math.Pow(c1.Imaginary, 2)));
        }
    }

}